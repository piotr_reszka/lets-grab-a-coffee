package com.reszka.piotr.models;

import com.reszka.piotr.validators.password.PasswordMatches;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "meeting")
public class Meeting implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @DateTimeFormat
    private LocalDateTime meetingDate;

//    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private User meetingCreator;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private User meetingGuest;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @NotNull
    private Place meetingPlace;

    @NotNull
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Place getMeetingPlace() {
        return meetingPlace;
    }

    public void setMeetingPlace(Place meetingPlace) {
        this.meetingPlace = meetingPlace;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getMeetingCreator() {
        return meetingCreator;
    }

    public void setMeetingCreator(User meetingCreator) {
        this.meetingCreator = meetingCreator;
    }

    public User getMeetingGuest() {
        return meetingGuest;
    }

    public void setMeetingGuest(User meetingGuest) {
        this.meetingGuest = meetingGuest;
    }

    public LocalDateTime getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(LocalDateTime meetingDate) {
        this.meetingDate = meetingDate;
    }
}


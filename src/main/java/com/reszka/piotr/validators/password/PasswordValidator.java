//package com.reszka.piotr.validators.password;
//
//import com.reszka.piotr.models.User;
//
//import javax.validation.ConstraintValidator;
//import javax.validation.ConstraintValidatorContext;
//
//public class PasswordValidator implements ConstraintValidator<PasswordMatches, Object> {
//    public void initialize(PasswordMatches passwordMatches) {
//    }
//
//    public boolean isValid(Object o, ConstraintValidatorContext context) {
//        User user = (User) o;
//        return user.getPassword().equals(user.getMatchingPassword());
//    }
//}

package com.reszka.piotr.validators.password;

import com.reszka.piotr.models.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by eclipse on 11.07.17.
 */
@Component
public class PasswordValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) {
        User user = (User)target;
        String password = user.getPassword();
        String confPassword = user.getMatchingPassword();

        if(!password.equals(confPassword)){
            errors.rejectValue("password","error.user", "Password not confirmed (mismatch)");
        }
    }
}
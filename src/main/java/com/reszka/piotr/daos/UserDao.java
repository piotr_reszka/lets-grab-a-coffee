package com.reszka.piotr.daos;

import com.reszka.piotr.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository("userRepository")
public interface UserDao extends JpaRepository<User, Long> {

    User findOneByEmail(String email);

}

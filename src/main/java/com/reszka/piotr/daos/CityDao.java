package com.reszka.piotr.daos;


import com.reszka.piotr.models.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository("cityRepository")
public interface CityDao extends JpaRepository<City, Long>{

}

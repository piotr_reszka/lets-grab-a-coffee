package com.reszka.piotr.daos;

import com.reszka.piotr.models.Meeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository("meetingRepository")
public interface MeetingDao extends JpaRepository<Meeting, Long> {
    Meeting findById(long id);
}

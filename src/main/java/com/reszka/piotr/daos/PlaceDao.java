package com.reszka.piotr.daos;


import com.reszka.piotr.models.City;
import com.reszka.piotr.models.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository("placeRepository")
public interface PlaceDao extends JpaRepository<Place, Long>{

}

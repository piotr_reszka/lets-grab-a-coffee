package com.reszka.piotr.controllers;

import com.reszka.piotr.models.City;
import com.reszka.piotr.models.User;
import com.reszka.piotr.services.impl.DefaultCityService;
import com.reszka.piotr.services.impl.DefaultUserService;
import com.reszka.piotr.validators.password.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    DefaultUserService defaultUserService;
    @Autowired
    DefaultCityService defaultCityService;
    @Autowired
    PasswordValidator passwordValidator;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView register() {
        ModelAndView model = new ModelAndView("registration");
        User user = new User();
        model.addObject("user", user);
        model.addObject("cities", defaultCityService.getAllCities());
        return model;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registerPost(@ModelAttribute("user") @Valid User user, BindingResult result,
                                     WebRequest request, Errors errors) {
        User userExists = defaultUserService.getUserByEmail(user.getEmail());
        passwordValidator.validate(user, result);
        if (userExists != null) {
            result
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided!");
        }
        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("registration");
            if (result.hasGlobalErrors()) modelAndView.addObject("passwordError", true);
            modelAndView.addObject("cities", defaultCityService.getAllCities());
            return modelAndView;

        } else {
            defaultUserService.addNewUser(user);
            return new ModelAndView("login");
        }
    }

}

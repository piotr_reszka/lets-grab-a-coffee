package com.reszka.piotr.controllers;

import com.reszka.piotr.models.Meeting;
import com.reszka.piotr.services.impl.DefaultMeetingService;
import com.reszka.piotr.services.impl.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HelloController {

    @Autowired
    DefaultUserService defaultUserService;
    @Autowired
    DefaultMeetingService defaultMeetingService;

    @RequestMapping(value = "/listOfEvents")
    public ModelAndView greeting() {
        ModelAndView model = new ModelAndView("listOfEvents");
        model.addObject("meetings", defaultMeetingService.getAllMeetings());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        model.addObject("localDateTimeFormat", formatter);
        return model;
    }

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and/or password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        return "login";
    }

    public List<String> getTimeOfEvents(){
        List<String> times = new ArrayList<>();
        String time;
        DateTimeFormatter fmtDay = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        DateTimeFormatter fmtTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
        List<Meeting> meetings = defaultMeetingService.getAllMeetings();
        for (Meeting m : meetings
                ) {
            LocalDateTime dt = m.getMeetingDate();
            int hour = dt.getHour();
            int minute = dt.getMinute();
            int second = dt.getSecond();
            LocalTime localTime = LocalTime.of(hour, minute, second);
            time = fmtDay.format(dt) + " " + fmtTime.format(localTime);
            times.add(time);
        }
        return times;
    }
}

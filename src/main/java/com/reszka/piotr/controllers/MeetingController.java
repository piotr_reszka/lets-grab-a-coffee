package com.reszka.piotr.controllers;

import com.reszka.piotr.models.Meeting;
import com.reszka.piotr.models.User;
import com.reszka.piotr.services.MeetingService;
import com.reszka.piotr.services.PlaceService;
import com.reszka.piotr.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping(value = "/meeting")
public class MeetingController {

    @Autowired
    private MeetingService meetingService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private UserService userService;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView meetingDetails(@RequestParam("id") long id, Principal principal) {
        ModelAndView model = new ModelAndView("meetingDetails");
        String principalMail = principal.getName();
        Meeting meeting = meetingService.getMeetingById(id);
        model.addObject("meeting", meeting);
        model.addObject("principalMail", principalMail);
        model.addObject("localDateTimeFormat", formatter);
        String meetingGuestEmail = "";
        try {
            meetingGuestEmail = meeting.getMeetingGuest().getEmail();
        } catch (NullPointerException np) {
        }
        String emptyString = "";
        model.addObject("meetingGuestEmail", meetingGuestEmail);
        model.addObject("emptyString", emptyString);
        return model;
    }

    @RequestMapping(value = "/join")
    public ModelAndView meetingDetailsPost(@RequestParam("id") long id, Principal principal) {

        ModelAndView model = new ModelAndView("meetingDetailsJoin");
        Meeting meeting = meetingService.getMeetingById(id);
        model.addObject("meeting", meeting);
        model.addObject("localDateTimeFormat", formatter);
        boolean added;
        added = meetingService.addGuestToMeeting(id, principal);
        model.addObject("status", added);

        return model;
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        Meeting meeting = new Meeting();
        ModelAndView model = new ModelAndView("meetingCreate");
        model.addObject("meeting", meeting);
        model.addObject("places", placeService.getAllPlaces());
        return model;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createPost(@ModelAttribute("meeting") @Valid Meeting meeting, BindingResult result,
                                   Principal principal) {
        ModelAndView model;
        if (result.hasErrors()) {
            model = new ModelAndView("meetingCreate");
            model.addObject("places", placeService.getAllPlaces());
            return model;

        } else {
            User user = userService.getUserByEmail(principal.getName());
            meeting.setMeetingCreator(user);
            meetingService.addNewMeeting(meeting);
            model = new ModelAndView("listOfEvents");
            model.addObject("localDateTimeFormat", formatter);
            model.addObject("meetings", meetingService.getAllMeetings());
            return model;
        }
    }


}

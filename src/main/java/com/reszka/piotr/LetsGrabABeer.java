package com.reszka.piotr;

import com.reszka.piotr.converters.LocalDateTimeConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class LetsGrabABeer extends WebMvcConfigurerAdapter {
    public static void main(String[] args) {
        SpringApplication.run(LetsGrabABeer.class, args);
    }

    @Override
    public void addFormatters(FormatterRegistry registry){
        registry.addConverter(new LocalDateTimeConverter("yyyy-MM-dd HH:mm"));
    }
}

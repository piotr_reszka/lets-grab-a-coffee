package com.reszka.piotr.converters;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeConverter implements Converter<String, LocalDateTime>{

    private final DateTimeFormatter formatter;

    public LocalDateTimeConverter(String dateFormat) {
        this.formatter = DateTimeFormatter.ofPattern(dateFormat);
    }

    @Override
    public LocalDateTime convert(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        }
        source+=":00";
        LocalDateTime dt = LocalDateTime.parse(source, DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        String formatted = dt.format(formatter);
        return LocalDateTime.parse(formatted, formatter);
    }
}

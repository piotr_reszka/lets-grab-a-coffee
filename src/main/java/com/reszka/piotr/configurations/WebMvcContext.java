package com.reszka.piotr.configurations;

import com.reszka.piotr.converters.LocalDateTimeConverter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

public class WebMvcContext extends WebMvcConfigurerAdapter {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateTimeConverter("MM/dd/yyyy h:mm a"));
    }

}

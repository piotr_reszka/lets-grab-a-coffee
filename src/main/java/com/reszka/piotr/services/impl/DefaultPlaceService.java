package com.reszka.piotr.services.impl;

import com.reszka.piotr.daos.PlaceDao;
import com.reszka.piotr.models.Place;
import com.reszka.piotr.services.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("defaultPlaceService")
@Transactional
public class DefaultPlaceService implements PlaceService {

    @Autowired
    PlaceDao placeDao;

    @Override
    public List<Place> getAllPlaces() {
        return placeDao.findAll();
    }

    @Override
    public Place getPlaceById(long id) {
        return placeDao.findOne(id);
    }
}

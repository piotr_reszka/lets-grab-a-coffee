package com.reszka.piotr.services.impl;

import com.reszka.piotr.daos.CityDao;
import com.reszka.piotr.models.City;
import com.reszka.piotr.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("defaultCityService")
@Transactional
public class DefaultCityService implements CityService {

    @Autowired
    CityDao cityDao;

    @Override
    public List<City> getAllCities() {
        return cityDao.findAll();
    }

    @Override
    public City getCityById(long id) {
        return cityDao.findOne(id);
    }
}

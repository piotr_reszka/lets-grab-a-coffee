package com.reszka.piotr.services.impl;

import com.reszka.piotr.daos.MeetingDao;
import com.reszka.piotr.models.Meeting;
import com.reszka.piotr.models.User;
import com.reszka.piotr.services.MeetingService;
import com.reszka.piotr.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;

@Service("defaultMeetingService")
@Transactional
public class DefaultMeetingService implements MeetingService {

    @Autowired
    MeetingDao meetingDao;
    @Autowired
    UserService userService;

    @Override
    public List<Meeting> getAllMeetings() {
        return meetingDao.findAll();
    }

    @Override
    public Meeting getMeetingById(long id) {
        return meetingDao.findOne(id);
    }

    @Override
    public boolean addGuestToMeeting(long id, Principal principal) {
        Meeting meeting = meetingDao.findById(id);
        User guest = meeting.getMeetingGuest();
        if(guest!=null) return false;
        String email = principal.getName();
        User user = userService.getUserByEmail(email);
        meeting.setMeetingGuest(user);
        meetingDao.save(meeting);
        return true;
    }

    @Override
    public Meeting addNewMeeting(Meeting meeting) {
        return meetingDao.save(meeting);
    }
}

package com.reszka.piotr.services;

import com.reszka.piotr.models.City;

import java.util.List;

public interface CityService {
    List<City> getAllCities();
    City getCityById(long id);
}

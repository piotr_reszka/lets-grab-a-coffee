package com.reszka.piotr.services;


import com.reszka.piotr.models.User;

public interface UserService {
    User getUserByEmail(String email);
    User addNewUser(User user);
}

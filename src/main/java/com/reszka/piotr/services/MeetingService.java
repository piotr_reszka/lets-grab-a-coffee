package com.reszka.piotr.services;

import com.reszka.piotr.models.Meeting;

import java.security.Principal;
import java.util.List;

public interface MeetingService {
    List<Meeting> getAllMeetings();
    Meeting getMeetingById(long id);
    boolean addGuestToMeeting(long id, Principal principal);
    Meeting addNewMeeting(Meeting meeting);
}

package com.reszka.piotr.services;

import com.reszka.piotr.models.City;
import com.reszka.piotr.models.Place;

import java.util.List;

public interface PlaceService {
    List<Place> getAllPlaces();
    Place getPlaceById(long id);
}

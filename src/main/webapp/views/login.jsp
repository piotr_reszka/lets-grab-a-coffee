<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>

<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log in with your account</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<style>
    .bg-1 {
        background-color: #205120; /* Green */
        color: #ffffff;
    }

    .bg-4 {
        background-color: #ffffff; /* Black Gray */
        color: #000000;
    }

    .navbar {
        padding-top: 15px;
        padding-bottom: 15px;
        border: 0;
        border-radius: 0;
        margin-bottom: 0;
        font-size: 12px;
        letter-spacing: 5px;
    }
</style>

<body style="height: 100%">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="\listOfEvents">Let's grab a coffee</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Log in</a></li>
                <li><a href="/registration">Register</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid bg-1" style="height: 80%">
    <div class="row">
        `>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <h2 class="form-heading">Log in</h2>
            <form method="POST" action="/login" class="form-signin">

                <div class="form-group ${error != null ? 'has-error' : ''}">
                    <div class="form-group">
                        <span>${message}</span>
                        <input name="username" type="email" class="form-control" placeholder="Your username (email)"
                               autofocus="true"/>
                    </div>

                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="Your password"/>
                        <span>${error}</span>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-lg btn-default btn-block" type="submit">Log in!</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
        </div>
    </div>
</div>
<footer class="container-fluid bg-4 text-center" style="height: 10%">
    <p>Contact: letsgrabacofee@gmail.com</p>
</footer>
</body>
</html>
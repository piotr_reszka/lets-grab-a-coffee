<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en" style="height: 100%">
<head>
    <title>Meeting created by: ${meeting.meetingCreator.firstName}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {
            font: 20px Lato, sans-serif;
            line-height: 1.8;
            color: #000000;
        }

        p {
            font-size: 16px;
        }

        .margin {
            margin-bottom: 45px;
        }

        .bg-1 {
            background-color: #205120; /* Green */
            color: #ffffff;
        }

        .bg-4 {
            background-color: #ffffff; /* Black Gray */
            color: #000000;
        }

        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }

        .navbar-nav li a:hover {
            color: #1abc9c !important;
        }
    </style>
</head>
<body style="height: 100%">

<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header" >
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="\listOfEvents">Let's grab a coffee</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/meeting/create">Create new event</a></li>
                <li><a href="/listOfEvents">List of events</a></li>
                <li><a href="/login">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- First Container -->
<div class="container-fluid bg-1">
    <div class="row">
        <div class="col-md-4 col-lg-4">
            <h1 class="margin" style="margin-left: 20px">Meeting created by: ${meeting.meetingCreator.firstName}</h1>
        </div>
        <div class="col-md-2 col-lg-2">
        </div>
        <c:if test="${meetingGuestEmail eq emptyString}">
            <c:if test="${principalMail ne meeting.meetingCreator.email}">
                <div class="col-md-6 col-lg-6" style="margin-top: 20px; margin-bottom: 15px">
                    <a href="/meeting/join?id=${meeting.id}" type="button" class="btn btn-default btn-block">Join this
                        event!</a>
                </div>
            </c:if>
        </c:if>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <%--photo--%>
            <img src="${meeting.meetingPlace.placeImageSrc}">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <table class="table table-hover table-dark" style="color: wheat;">
                <thead>
                <tr>
                    <th scope="col">Meeting creator</th>
                    <th scope="col">${meeting.meetingCreator.firstName}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Meeting time</td>
                    <td>${meeting.meetingDate.format(localDateTimeFormat)}</td>
                </tr>
                <tr>
                    <td>Where?</td>
                    <td>${meeting.meetingPlace.street}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <%--description--%>
            ${meeting.description}
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="container-fluid bg-4 text-center" style="height: 10%">
    <p>Contact: letsgrabacofee@gmail.com</p>

</footer>

</body>
</html>

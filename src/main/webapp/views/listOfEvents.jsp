<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Events in Wroclaw</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {
            font: 20px Lato, sans-serif;
            line-height: 1.8;
            color: #f5f6f7;
        }

        p {
            font-size: 16px;
        }

        .margin {
            margin-bottom: 45px;
        }

        .bg-1 {
            background-color: #205120; /* Green */
            color: #ffffff;
        }

        .bg-4 {
            background-color: #ffffff; /* Black Gray */
            color: #000000;
        }


        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }

        .navbar-nav li a:hover {
            color: #1abc9c !important;
        }
        @media only screen
        and (min-device-width: 375px)
        and (max-device-width: 667px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .media-size {
                width: 140px;
            }
        }
        .media-size {
            width: 200px;
        }
    </style>
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="\listOfEvents">Let's grab a coffee</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/meeting/create">Create new event</a></li>
                <li><a href="/listOfEvents">List of events</a></li>
                <li><a href="/login">Logout</a></li>

            </ul>
        </div>
    </div>
</nav>

<!-- First Container -->
<div class="container-fluid bg-1">
    <h2 class="margin" style="margin-left: 20px">Newest events in Wroclaw</h2>
    <div class="list-group">
        <c:forEach items="${meetings}" var="meeting">
            <a href="/meeting?id=${meeting.id}" class="list-group-item" >
                <div class="media">
                    <div class="media-left">
                        <img src="${meeting.meetingPlace.placeImageSrc}" class="media-object media-size">
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading">${meeting.meetingCreator.firstName}, ${meeting.meetingDate.format(localDateTimeFormat)}, ${meeting.meetingPlace.placeName}</h3>
                        <h4>
                            ${meeting.description}
                        </h4>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>

<!-- Footer -->
<footer class="container-fluid bg-4 text-center">
    <p>Contact: letsgrabacofee@gmail.com</p>
</footer>

</body>
</html>

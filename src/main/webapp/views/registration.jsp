<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>

<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Register!</title>
</head>
<style>
    .bg-1 {
        background-color: #205120; /* Green */
        color: #ffffff;
    }

    .bg-4 {
        background-color: #ffffff; /* Black Gray */
        color: #000000;
    }

    .navbar {
        padding-top: 15px;
        padding-bottom: 15px;
        border: 0;
        border-radius: 0;
        margin-bottom: 0;
        font-size: 12px;
        letter-spacing: 5px;
    }
</style>

<body style="height: 100%">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="\listOfEvents">Let's grab a coffee</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Log in</a></li>
                <li><a href="/registration">Register</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid bg-1" style="height: 80%">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="form-heading">Register</h2>
        </div>
    </div>
    <form:form method="post" action="registration" modelAttribute="user">
        <div class="form-group">
            <form:input type="email" class="form-control" id="login" placeholder="Your email address" path="email"/>
            <form:errors path="email"></form:errors>
        </div>
        <div class="form-group">
            <form:input type="password" class="form-control" id="password" placeholder="Password" path="password"/>
            <form:errors path="password"></form:errors>
            <c:forEach var="error" items="${errors}">
            </c:forEach>
        </div>
        <div class="form-group">
            <form:input type="password" class="form-control" id="confirmPassword" placeholder="Repeat your password"
                        path="matchingPassword"/>
            <form:errors path="matchingPassword"></form:errors>
            <c:if test="${passwordError}">Password doesn't match</c:if>

        </div>
        <div class="form-group">
            <form:input type="text" class="form-control" id="firstName" placeholder="Your first name" path="firstName"/>
            <form:errors path="firstName"></form:errors>
        </div>
        <div class="form-group">
            <form:input type="text" class="form-control" id="lastName" placeholder="Your last name" path="lastName"/>
            <form:errors path="lastName"></form:errors>
        </div>
        <div class="form-group">
            <form:input path="age" class="form-control" id="age" placeholder="Your age"/>
            <form:errors path="age"></form:errors>
        </div>
        <div>
            <form:select path="city" class="form-control">
                <form:options items="${cities}" itemLabel="name" />
            </form:select>
        </div>
        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg btn-block">Register!</button>
        </div>
    </form:form>
</div>
<footer class="container-fluid bg-4 text-center" style="height: 10%">
    <p>Contact: letsgrabacofee@gmail.com</p>
</footer>
</body>
</html>
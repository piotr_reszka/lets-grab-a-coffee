<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>

<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Create new event</title>
</head>
<style>
    .bg-1 {
        background-color: #205120; /* Green */
        color: #ffffff;
    }

    .bg-4 {
        background-color: #ffffff; /* Black Gray */
        color: #000000;
    }

    .navbar {
        padding-top: 15px;
        padding-bottom: 15px;
        border: 0;
        border-radius: 0;
        margin-bottom: 0;
        font-size: 12px;
        letter-spacing: 5px;
    }
</style>

<body style="height: 100%">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="\listOfEvents">Let's grab a coffee</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/meeting/create">Create new event</a></li>
                <li><a href="/listOfEvents">List of events</a></li>
                <li><a href="/login">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid bg-1" style="height: 80%">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="form-heading">Create an event</h2>
        </div>
    </div>
    <form:form method="post" action="create" modelAttribute="meeting">
        <div class="form-group">
            <form:input type="datetime-local" id="dupa" class="form-control" name="dupa" placeholder="Date of meeting" path="meetingDate"/>
            <%--<form:hidden path="meetingDate" value="${localDateTimeFormat.parse(request.getParameter('dupa'))}"></form:hidden>--%>
            <form:errors path="meetingDate"></form:errors>
        </div>
        <div class="form-group">
            <form:input path="description" class="form-control" id="description" placeholder="Provide some description here" />
            <form:errors path="description"></form:errors>
        </div>
        <div>
            <form:select path="meetingPlace" class="form-control">
                <form:options items="${places}" itemLabel="placeName" />
            </form:select>
        </div>
        <br>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-default btn-block">Create new event</button>
        </div>
    </form:form>
</div>
<footer class="container-fluid bg-4 text-center" style="height: 10%">
    <p>Contact: letsgrabacofee@gmail.com</p>
</footer>
</body>
</html>
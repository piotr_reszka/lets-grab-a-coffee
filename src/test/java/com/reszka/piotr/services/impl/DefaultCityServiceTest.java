package com.reszka.piotr.services.impl;

import com.reszka.piotr.daos.CityDao;
import com.reszka.piotr.models.City;
import com.reszka.piotr.services.CityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.BDDMockito.*;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DefaultCityServiceTest {

    @Configuration
    static class DefaultCityServiceTestConf {
        @Mock
        CityDao cityDao;
        @Bean
        public DefaultCityService cityService() {
            return new DefaultCityService();
        }
        @Bean
        public CityDao cityDao() {
            return cityDao;
        }
    }


    @Autowired
    private CityDao cityDao;
    @Autowired
    private CityService cityService;
    @Mock
    City city;

    @Before
    public void setUp(){


//                        cityDao.save(city);
    }


    @Test
    public void shouldReturnOneCity() throws Exception {
        ArrayList<City> cities = new ArrayList<City>();
        List<City> citiesList = cityDao.findAll();
        for (City c : citiesList
             ) {
            cities.add(c);
        }
        assertEquals(1, cities.size());
    }

    @Test
    public void getCityById() throws Exception {
    }

}